const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./app/config');
const db = require('./app/models');
const PORT = process.env.PORT || 3000;
const app = express();

const https = require('https');
const fs = require('fs');

app.use(cors());
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/card.routes')(app);

// Certificate SSL
// const privateKey = fs.readFileSync('/etc/letsencrypt/live/sct-api.redigames.com/privkey.pem', 'utf8');
// const certificate = fs.readFileSync('/etc/letsencrypt/live/sct-api.redigames.com/cert.pem', 'utf8');
// const ca = fs.readFileSync('/etc/letsencrypt/live/sct-api.redigames.com/chain.pem', 'utf8');

db.mongoose
	.connect(config.connectionString, {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})
	.then(() => {
		console.log('Successfully connect to MongoDB.');

        // SSL
		// https
		// 	.createServer(
		// 		{
		// 			key: privateKey,
		// 			cert: certificate,
		// 			ca: ca
		// 		},
		// 		app
		// 	)
		// 	.listen(PORT, () =>
		// 		console.log(`Server is running on SSL on port ${PORT}.`)
		// 	);

        // HTTP
		app.listen(PORT, () =>
			console.log(`Server is running on port ${PORT}.`)
		);
	})
	.catch((err) => {
		console.error('Connection error', err);
		process.exit();
	});
