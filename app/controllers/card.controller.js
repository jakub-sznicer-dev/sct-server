const config = require('../config');
const db = require('../models');
const Card = db.card;
var ObjectID = require('mongodb').ObjectID;

exports.addCard = (req, res) => {
	const card = new Card({
		name: req.body.name,
		text: req.body.text,
		consequences: req.body.consequences,
		resource: req.body.resource,
		score: [],
		owner: req.userId
	});

	card.save((err, card) => {
		if (err) {
			res.status(500).send({ message: err });
			return;
		}

		res.status(200).send({ id: card.id });
	});
};

exports.getCards = async (req, res) => {
	let page = req.params.page;
	cards = await Card.paginate(
		{},
		{
			page: page,
			limit: 10,
			populate: [
				{ path: 'owner', options: { select: 'username' } },
				{ path: 'userScore', options: { select: '_id' } }
			]
		}
	);
	res.send(cards);
};

exports.likeCard = async (req, res) => {
    let card = await Card.findById(req.params.cardId);

    if (!card.userScore) card.userScore = [];

    let cardScored = card.userScore.find(e => e._id == req.userId);

    if (cardScored) {
        card.userScore = card.userScore.filter(e => e._id != req.userId);
    } else {
        card.userScore.push(new ObjectID(req.userId));
    }

	await card.save((err, card) => {
		if (err) {
			res.status(500).send({ message: err });
			return;
		}

		res.status(200).send(cardScored);
	});
};
