const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = mongoose.model(
	'User',
	new mongoose.Schema({
		username: String,
		email: String,
        password: String,
        cards: [{
            type: Schema.Types.ObjectId,
            ref: 'Card'
        }]
	})
);

module.exports = User;
