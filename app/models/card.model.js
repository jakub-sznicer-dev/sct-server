const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

let cardSchema = new mongoose.Schema({
	name: String,
	text: String,
	consequences: String,
	resource: String,
	userScore: [{
		type: Schema.Types.ObjectId,
		ref: 'User'
	}],
	owner: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	}
});

cardSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Card', cardSchema);
