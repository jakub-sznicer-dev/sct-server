const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {
	mongoose: mongoose,
    user: require('./user.model'),
    card: require('./card.model')
};

module.exports = db;
